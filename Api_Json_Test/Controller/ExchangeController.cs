﻿using Api_Json_Test.DTOs;
using Api_Json_Test.Entity;
using Api_Json_Test.HttpClientCommunication;
using Api_Json_Test.IRepositoy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Json_Test.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeController : ControllerBase
    {
        private readonly IGenericRepository<Exchange> repository;
        private readonly OpenexchangeratesCommunication  _exchangeRepository;
        #region Constructor
        public ExchangeController(IGenericRepository<Exchange> repository, OpenexchangeratesCommunication exchangeRepo)
        {
            this.repository = repository;
            _exchangeRepository = exchangeRepo;

        }
        #endregion

        //Write backend code that gets exchange rates for EUR and GBP and store it in the database, with date and exchange rates for EUR and GBP.
        [HttpGet]
        public DataEntityDTO GetExchange(DateTime date)
        {
            DataEntityDTO ExchangeData = _exchangeRepository.GetListExchange(date).Result;
          
            Exchange exchange = new Exchange();
            exchange.EUR = ExchangeData.Rates.EUR;
            exchange.GBP = ExchangeData.Rates.GBP;
            exchange.Date = date;

            string AddData = repository.Add(exchange);

            return ExchangeData;
        }


        //Calculate the 7-days moving average
        [HttpGet("CalculateMA")]
        public AverageExchangeRateDTO GetLastExchange()
        {
            DateTime date = DateTime.Now;
            List<Exchange> ListOfLastExchange = new List<Exchange>();

            for (int i = 1; i < 8; i++)
            {   var Lastdate = date.AddDays(-i)   ;
                DataEntityDTO ExchangeData = _exchangeRepository.GetListExchange(Lastdate).Result;
                Exchange exchange = new Exchange();
                exchange.EUR = ExchangeData.Rates.EUR;
                exchange.GBP = ExchangeData.Rates.GBP;
                exchange.Date = date;
                ListOfLastExchange.Add(exchange);
            }
            AverageExchangeRateDTO avrg = new AverageExchangeRateDTO();
            avrg.AvrgEUR = ListOfLastExchange.Select(x => x.EUR).Average();
            avrg.AvrgGBP = ListOfLastExchange.Select(x => x.GBP).Average();
            return avrg;
        }

        //Calculate the 7-day exponential moving average 
        [HttpGet("CalculateEMA")]
        public List<Rate> CalculateEMA()
        {

            DateTime date = DateTime.Now;
            var Eur = new List<decimal>();
            var Gbp = new List<decimal>();
            for (int j = 1; j < 8; j++)
            {
                var Lastdate = date.AddDays(-j);
                DataEntityDTO ExchangeData = _exchangeRepository.GetListExchange(Lastdate).Result;

                Eur.Add(ExchangeData.Rates.EUR);
                Gbp.Add(ExchangeData.Rates.GBP);
            }

            decimal k = (decimal)(2.0 / (7 + 1));

            var y = new List<Rate>();

            var a = new Rate();
            a.EUR = Eur[0];
            a.GBP = Gbp[0];
            y.Add(a);

            for (int i = 1; i < 7; i++)
            {
                var b = new Rate();
                b.EUR = k * Eur[i] + (1 - k) * y[i - 1].EUR;
                b.GBP = k * Gbp[i] + (1 - k) * y[i - 1].GBP;
                y.Add(b);
            }

            return y;
        }

    }
}
