﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Json_Test.Entity
{
    public class Exchange
    {
        public Guid ExchangeId { get; set; }
        public decimal EUR{ get; set; }
        public decimal GBP { get; set; }
        public DateTime Date { get; set; }
       
    }
}
