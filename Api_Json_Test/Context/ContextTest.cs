﻿using Api_Json_Test.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Json_Test.Context
{
    public class ContextTest : DbContext
    {
        public ContextTest(DbContextOptions<ContextTest> options) : base(options) { }

        public DbSet<Exchange> Exchange { get; set; }
      

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Settings: set Primary keys

            modelBuilder.Entity<Exchange>()
                         .HasKey(c => c.ExchangeId);

            #endregion

            

        }
    }
}
