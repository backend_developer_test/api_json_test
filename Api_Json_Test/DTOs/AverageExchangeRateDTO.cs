﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Json_Test.DTOs
{
    public class AverageExchangeRateDTO
    {
      
        public decimal AvrgEUR { get; set; }
        public decimal AvrgGBP { get; set; }
      
    }
}
