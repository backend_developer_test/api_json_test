﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_Json_Test.DTOs
{
    public class DataEntityDTO
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public int Timestamp { get; set; }
       // public string bases { get; set; }
        public Rate Rates { get; set; }
    }
    public class Rate 
    {
        //public string currency { get; set; }
        public decimal EUR { get; set; }
        public decimal GBP { get; set; }
    }
}
