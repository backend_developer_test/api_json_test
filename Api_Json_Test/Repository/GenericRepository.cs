﻿using Api_Json_Test.Context;
using Api_Json_Test.IRepositoy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Api_Json_Test.Repository
{

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly ContextTest context;
        private readonly DbSet<TEntity> table = null;

        #region Constructor
        public GenericRepository(ContextTest context)
        {
            this.context = context;
            table = this.context.Set<TEntity>();
        }
        #endregion

        #region Create Funtion
        public string Add(TEntity entity)
        {
            try
            {
                table.Add(entity);
                context.SaveChanges();
                return "Added Done";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion


        #region Read Function
        public TEntity Get(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes = null)
        {
            try
            {
                IQueryable<TEntity> query = table;

                if (includes != null)
                {
                    query = includes(query);
                }
                if (condition != null)
                    return query.FirstOrDefault(condition);
                else
                    return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includes = null)
        {
            try
            {
                IQueryable<TEntity> query = table;

                if (includes != null)
                {
                    query = includes(query);
                }
                if (condition != null)
                    return query.Where(condition).ToList();

                else
                    return query.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }
        #endregion

        #region Update Function
        public string Put(TEntity entity)
        {
            try
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                return "Update Done";

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
        #endregion

        #region Remove Function
        public string Remove(Guid id)
        {
            try
            {
                TEntity entity = table.Find(id);
                if (entity == null)
                    return "Not Found";
                else
                {
                    table.Remove(entity);
                    context.SaveChanges();
                    return "Delete Done";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion
    }
}
