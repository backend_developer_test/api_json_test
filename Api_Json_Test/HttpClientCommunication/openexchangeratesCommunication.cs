﻿using Api_Json_Test.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Api_Json_Test.HttpClientCommunication
{
    public class OpenexchangeratesCommunication
    {

        private readonly IHttpClientFactory _httpClientFactory;

        public OpenexchangeratesCommunication(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }

        public async Task <DataEntityDTO> GetListExchange(DateTime date)
        {
            var dateConverted = date.ToString("yyyy-MM-dd");
            var httpClient = _httpClientFactory.CreateClient("ExchangeAPI");
            var response = await httpClient.GetAsync($"historical/"+ dateConverted +".json?app_id=b754865f068846cbbfacb0eb99ea007d&show_alternative=false&prettyprint=false");
            string responseStream = response.Content.ReadAsStringAsync().Result;

            try
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataEntityDTO>(responseStream);

                return await Task.FromResult(data);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //public async Task<dataEntityDTO> GetListLastExchange(DateTime date)
        //{
        //    var dateConverted = date.ToString("yyyy-MM-dd");
        //    var httpClient = _httpClientFactory.CreateClient("ExchangeAPI");
        //    var response = await httpClient.GetAsync($"historical/" + dateConverted + ".json?app_id=b754865f068846cbbfacb0eb99ea007d&show_alternative=false&prettyprint=false");
        //    string responseStream = response.Content.ReadAsStringAsync().Result;

        //    try
        //    {
        //        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<dataEntityDTO>(responseStream);

        //        return await Task.FromResult(data);
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}
        //public async Task<dataEntityDTO> GetAveregeExchange(DateTime starttime)
        //{
        //   var dateConverted = starttime.ToString("yyyy-MM-dd");
        //    var httpClient = _httpClientFactory.CreateClient("ExchangeAPI");
        //    var response = await httpClient.GetAsync($"ohlc.json?app_id=b754865f068846cbbfacb0eb99ea007d&start_time="+ starttime + "&period=7d&base="+ bases + "&symbols="+symbols+ "&prettyprint=true");
        //    string responseStream = response.Content.ReadAsStringAsync().Result;

        //    try
        //    {
        //        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<dataEntityDTO>(responseStream);

        //        return await Task.FromResult(data);
        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

    }
}
